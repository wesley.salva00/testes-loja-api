### Collection - Postman

Nesta collection estão os testes de criação dos produtos e buscando os produtos cadastrados

## Execução
- É necessário ter o postman instalado
- A api precisa estar rodando
- Basta abrir o postman, importar a collection e clicar em RUN collection

## Observação de execução

- A api não tem um endpoint para retornar os ids dos produtos cadastrados, então os 
testes de busca dos produtos funcionam assim que serviço é inicializado no docker,
pois os ids são atribuídos sequencialmente, porém quando os produtos são deletados os 
ids permanecem, quando novos produtos são criados os ids são atribuídos a partir
do último cadastrado independente se o produto foi excluído ou não.

### Para executar novamente a suíte de testes completa sem a necesseidade de editar, é necessário excluir os containers atuais e subir a api novamente:

docker ps -l
docker rm <container_id>
- Após isso basta subir a api novamente

### Ou editar a collection atribuindo os novos ids, que podem ser obtidos clicando no produto cadastrado pelo próprio navegador.

Logar no site > Produtos > Selecionar produto > Verificar o id_produto na url

